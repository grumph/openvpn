if [ $# -eq 0 ] # should check for no arguments
then
    echo "Usage: `basename $0` <NAME>"
    exit $E_OPTERROR
fi

NAME=$1



DIR=confs/$NAME


echo "* creation du repertoire"
mkdir -p $DIR
echo "* creation de la config client"
cp client.conf $DIR/capcat.ovpn
chmod +w $DIR/capcat.ovpn
echo "* creation de la cle"
cd ssl
. ./vars
./build-key $NAME
cd -
echo "* copie des fichiers"
cp ssl/keys/ca.crt $DIR
cp ssl/keys/$NAME.key $DIR/client.key
cp ssl/keys/$NAME.crt $DIR/client.crt
cp ssl/keys/tlsauth.key $DIR/tlsauth.key
echo "* creation de l'archive"
cd $DIR
tar cvzf $NAME.tar.gz ca.crt client.key client.crt capcat.ovpn tlsauth.key
echo "*FINI*"