import string, cgi
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import re

import ipAuth
import openvpnwrapper

ovpn = openvpnwrapper.OpenVPNWrapper('/etc/openvpn')
mypath = '/etc/openvpn/webserver/'
ipauth = ipAuth.ipAuth(100)

class MyHandler(BaseHTTPRequestHandler):
	def __init__(self, *args):
		BaseHTTPRequestHandler.__init__(self, *args)

	def sendTextHeaders(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()

	def check_auth(self):
		(ip, port) = self.client_address
		if ipauth.isAllowed(ip, "") and self.headers.getheader('Authorization') == 'Basic dGVzdDp0b3Rv': # base64.encodestring('test:toto')[:-1]
			ipauth.add(ip, "")
			return True
		else:
			if self.headers.getheader('Authorization') != 'Basic dGVzdDp0b3Rv':
				print "DEBUG : wrong header : ", self.headers.getheader('Authorization')
			elif not ipauth.isAllowed(ip, ""):
				print "DEBUG : ip not allowed"
			else:
				print "DEBUG : DAFUK ??!?!"
			self.send_response(401)
			self.send_header('WWW-Authenticate', 'Basic realm=\"Who are you ?\"')
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			self.wfile.write('Not allowed, try again later.')
			ipauth.add(ip, "")
			return False

	def print_info(self):
		print "client address = ", self.client_address
		print "server = ", self.server
		print "command = ", self.command
		print "path = ", self.path
		print "request version = ", self.request_version
		print "headers = ", self.headers
		print "version = ", self.version_string()
		print "address string = ", self.address_string()
		print "time = ", self.log_date_time_string()
		self.sendTextHeaders()
		self.wfile.write('OK')


	def print_register(self, error=''):
		try:
			f = open(mypath + 'register.html')
			self.sendTextHeaders()
			self.wfile.write(string.replace(f.read(), 'ERROR', error))
			f.close()
		except IOError:
			self.send_error(500, "Can't read the page")

	def print_recover(self, error=''):
		try:
			f = open(mypath + 'recover.html')
			self.sendTextHeaders()
			self.wfile.write(string.replace(f.read(), 'ERROR', error))
			f.close()
		except IOError:
			self.send_error(500, "Can't read the page")

	def send_config(self, name):
		(size, archive) = ovpn.get_conf(name)
		if size > 0:
			self.send_response(200)
			self.send_header('Content-type', 'application/gzip')
			self.send_header('Content-Length', size)
			self.send_header('Content-Disposition', 'attachment; filename="%s.tar.gz"' % name)
			self.end_headers()
			self.wfile.write(archive)
		else:
			self.send_error(404, "Conf file not found")



	def check_name(self, name, should_exist):
		check = re.compile("^[\w-]{3,}$") # no special characters except - and _, 3 chars at least
		if check.match(name) == None:
			return False
		exists = ovpn.conf_exists(name)
		return should_exist == exists

	def log_hacking_attempt(self):
		log_message('HACKING ATTEMPT')

	def do_GET(self):
		if self.check_auth():
			if self.path == "/register":
				self.print_register()
			elif self.path == "/recover":
				self.print_recover()
			elif (self.path == "/info"):
				self.print_info()
			else:
				self.send_error(404,'Page Not Found')

	def do_POST(self):
		if self.check_auth():
			ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
			if self.path == '/register' or self.path == '/recover': # page register with text posted
				if ctype == 'application/x-www-form-urlencoded' :
					length = int(self.headers.getheader('content-length'))
					postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
					name = postvars['name'][0]
					if self.check_name(name, (self.path == '/recover')): # if recovery mode, check existing name
						if self.path == '/register':
							if not ovpn.generate_conf(name):
								self.send_error(500, "Problem with script")
								return
						self.send_config(name)
					else :
						if self.path == '/register':
							self.print_register("Invalid nickname, please try again.")
						elif self.path == '/recover':
							self.print_recover("Invalid nickname, please try again.")
				else :
					self.log_hacking_attempt()
					self.send_error(400,'Bad request')
			else :
				self.send_error(404,'Page Not Found')

def main():
	try:
		server = HTTPServer(('', 80), MyHandler)
		print 'started httpserver...'
		server.serve_forever()
	except KeyboardInterrupt:
		print '^C received, shutting down server'
		server.socket.close()

if __name__ == '__main__':
	main()

