import time


class ipAuth():
	def __init__(self, expire=180):
		self.db = []
		self.EXPIRE_TIME = expire #time needed before expiration of an authorization

	def add(self, ip, f):
		#add ip allowed to get file f
		self.remove(ip)
		ts = time.time()
		self.db.append((ip, f, ts))
		print '[ipAuth] Allowed %s to get %s (%i)' % (ip, f, ts)

	def remove(self, ipaddr):
		try:
			for elt in [(ip, f, ts) for (ip, f, ts) in self.db if (ipaddr == ip)]:
				self.db.remove(elt)
				print '[ipAuth] Removed %s auth for %s (%i)' % elt
		except IndexError:
			return

	def gc(self):
		#garbage collect too old ip
		now = time.time()
		try:
			for elt in [(ip, f, ts) for (ip, f, ts) in self.db if (ts < (now - self.EXPIRE_TIME))]:
				self.db.remove(elt)
				print '[ipAuth] Garbage collected %s for %s (%i)' % elt
		except IndexError:
			return

	def isAllowed(self, ip, f):
		#return true if ip is allowed to access file f
		self.gc()
		for (ipaddr, fil, ts) in self.db:
			if ipaddr == ip and f == fil:
				print '[ipAuth] %s trying to access %s and allowed' % (ip, f)
				return True
		print '[ipAuth] WARNING : %s trying to access %s without authorization' % (ip, f)
		return False
