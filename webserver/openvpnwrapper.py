import pexpect
import os


class OpenVPNWrapper():
	def __init__(self, path):
		self.path = path

	def get_conf_filename(self, name):
		return self.path + os.sep + 'confs' + os.sep + name + os.sep + name + '.tar.gz'

	def get_conf(self, name):
		try:
			filename = self.get_conf_filename(name)
			f = open(filename, 'rb')
			result = f.read()
			f.close()
			return (self.get_conf_size(name), result)
		except:
			print "WARNING : a problem occured when reading the conf file"
			return (0, "")

	def get_conf_size(self, name):
		return os.path.getsize(self.get_conf_filename(name))

	def conf_exists(self, name):
		return os.access(self.get_conf_filename(name), os.R_OK)

	def generate_conf(self, name):
		print "DEBUG : running conf script"
		try:
			child = pexpect.spawn('sh create_client.sh %s' % name, timeout=5, cwd=self.path)
			child.expect_exact('Country Name (2 letter code) [NA]:')
			child.sendline('')
			child.expect_exact('State or Province Name (full name) [NA]:')
			child.sendline('')
			child.expect_exact('Locality Name (eg, city) [NA]:')
			child.sendline('')
			child.expect_exact('Organization Name (eg, company) [capcat.net]:')
			child.sendline('')
			child.expect_exact('Organizational Unit Name (eg, section) [openvpn]:')
			child.sendline('')
			child.expect_exact("Common Name (eg, your name or your server's hostname) [%s]:" % name)
			child.sendline('')
			child.expect_exact('Name [binouze]:')
			child.sendline(name)
			child.expect_exact('Email Address [admin@capcat.net]:')
			child.sendline('')
			child.expect_exact('A challenge password []:')
			child.sendline('')
			child.expect_exact('An optional company name []:')
			child.sendline('')
			child.expect_exact('Sign the certificate? [y/n]:')
			child.sendline('y')
			child.expect_exact('1 out of 1 certificate requests certified, commit? [y/n]')
			child.sendline('y')
			child.expect(pexpect.EOF)
			return not child.isalive()
		except (pexpect.EOF, pexpect.TIMEOUT):
			print "TODO : clean everything and kill child"
			print os.getcwd()
			print '== ERROR running script. Dumping : =='
			print child.before
			print '== Dump done =='
			return False
