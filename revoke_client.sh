if [ $# -eq 0 ] # should check for no arguments
then
    echo "Usage: `basename $0` <NAME>"
    exit $E_OPTERROR
fi

NAME=$1



DIR=confs/$NAME

echo "* revocation du certificat"
cd ssl
. ./vars
./revoke-full $NAME
cd -
echo "* suppression de la config client"
NUM=0
while [ -d "oldconfs/"$NAME"-"$NUM ]; do
    NUM=$(( $NUM + 1 ))
done
mv $DIR "oldconfs/"$NAME"-"$NUM
echo "*FINI*"
